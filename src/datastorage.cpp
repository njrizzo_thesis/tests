/*
 * 
 * DataStorage: module to manager a data set of values to save on 
 * SQLite database.
 * @Version: 1.0.0
 * @Author: Nilton José Rizzo
 * 
 * 
*/

#include <iostream>
#include <string>
#include <vector>
#include <sqlite3.h>

#include "main.hpp"

using namespace std;

static int callback(void *NotUsed, int argc, char **argv, char **azColName);


struct
Data {
	string name;
	string value;
	string type;
};

bool 	debugflag;
string	debugstr;


class 
DataStorage
{

private:

		string							dbname;
		string							name;
		vector<struct Data>				data;
		vector<struct Data>::iterator 	it;
		int								last;
		sqlite3							*db;
	
		int			create_table(sqlite3 *db, string sql);
		sqlite3		*initDataBase(string dbname, sqlite3 *db);
		int 		writeTable(sqlite3 * ldb, vector<Data> data);
		
public:

		DataStorage(string name="./data.sqlite3");
		~DataStorage();
		int storage(string name, string value, string type);
		int save(void);
		int size(void);
		int getPos(void) { return last; }
		Data *getLastElement(void);
		Data *getElement(int elem);
		Data *getElement(void);
		void setImageName(string n) { this->name = n; }
		
};


DataStorage::~DataStorage()
{
int r;

	if ( this->db != (sqlite3 *)(NULL) )
	{
		r = SQLITE_BUSY;
		while( r == SQLITE_BUSY ){
			r = sqlite3_close(this->db);
		}
		this->db = (sqlite3 *)(NULL); 
	}
	
	this->data.clear();
	this->last = 0;
	this->name = "-";
}



DataStorage::DataStorage(string name)
{
	this->dbname = name; 
	this->last = 0; 
	this->name = "-";
	this->db=initDataBase(this->dbname, this->db);
	if (  this->db == nullptr)
	{
		ERRMSG("Constructor", "Faild to open database file");
	}
}


Data *
DataStorage::getElement(void)
{
Data 	*a;

	if ( (this->last >= 0) && (this->last < this->data.size()-1) )
	{
		this->last++;
		a = &this->data[this->last];
	}
	else
		a = (Data *)(NULL);
	return a;
}

Data *
DataStorage::getElement(int elem)
{
Data	*a;

	if ( elem >= 0 && elem < this->data.size() )
	{
		this->last = elem;
		a = &this->data[this->last];
	}
	else
		a = (Data *)(NULL);
	return a;
}

Data *
DataStorage::getLastElement(void)
{
int s;
Data 	*a;

	if ( this->data.size() )
	{
		s = this->data.size() - 1;
		a = &this->data[s];
	}
	else
		a = (Data *)(NULL);
	return a;
}

int
DataStorage::size(void)
{
	return this->data.size();
}

int
DataStorage::storage(string name, string value, string type)
{
struct Data		aux;

	aux.name = name;
	aux.value = value;
	aux.type = type;
	this->data.push_back(aux);
	this->last = this->data.size();
	return this->last;
}

int
DataStorage::save(void)
{
	if (this->name == "-" )
	{
		ERRMSG("initDataBase: Error:", "Image name is NULL. Set this");
		return -1;
	}
	return this->writeTable(this->db, this->data);
}


int
DataStorage::create_table(sqlite3 *db, string sql)
{
char *zErrMsg = 0;
int r;


	r = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	if ( r != SQLITE_OK ){
		ERRMSG("CREATE_TABLE", "Creating TABLE failed: SQL "+sql);
	}
	return r;
}

int
DataStorage::writeTable(sqlite3 *ldb, vector<Data> data)
{
string 		sql;
int 		vlen, r;
char 		*zErrMsg = 0;

	if ( sqlite3_db_readonly(ldb, this->name.c_str() ) == 1 )
		cout << "READONLY database" << endl;
	sql = "Insert into dado ( id_img, variavel, valor, tipo ) Values ( ";
	vlen = data.size();
	for (int c=0; c < vlen; c++)
	{
		sql =  sql + "\"" + this->name + "\",\"" + 
							this->data[c].name  + "\",\"" + 
							this->data[c].value + "\",\"" + 
							this->data[c].type  + "\")"; 
		if ( c != (vlen - 1) )
			sql += ",(";
	}
	DEBUGM("writeTable:", "Query = ", sql.c_str(), " ","","","");
	r = sqlite3_exec(ldb, sql.c_str(), callback, 0, &zErrMsg);
	if ( r != SQLITE_OK )
		ERRMSG("writeTable", "Insert into [ " + this->name + " ] failed: " + sqlite3_errmsg(ldb));
	return r;

}

sqlite3 *
DataStorage::initDataBase(string dbname, sqlite3 *ldb)
{
int 		r;
string		table;


	if ( (r = sqlite3_open(dbname.c_str(), &ldb) ) != SQLITE_OK)
	{
		ERRMSG("initDataBase ", "Create_Database: " + to_string(r) + " :" + sqlite3_errmsg(ldb));
		ldb = nullptr;
	}
	else
	{
		table = "CREATE table if not exists dado (\
					id			INTEGER PRIMARY KEY AUTOINCREMENT,\
					id_img		varchar(255) not null,\
					variavel 	varchar(64) not null,\
					valor		varchar(255) not null,\
					tipo		varchar(20) not null);";
		if ( (r = this->create_table(ldb, table))!= SQLITE_OK)
		{
			string a = to_string( r );
			string b = sqlite3_errmsg(ldb);
			ERRMSG("initDataBase", "table [dado] SQLite Error Code: " + a + " : " + b );
			ldb = nullptr;
		}
	}
	return ldb;
}


static int 
callback(void *NotUsed, int argc, char **argv, char **azColName)
{

	return 0;
}



int
main(void)
{
	DataStorage	d;
	Data		*sd;
	
	debugflag = false;
	debugstr = "DEBUG>>>";
	
	d.setImageName("teste.jpg");
	d.storage("Media", "10", "double");
	d.storage("Total", "167", "int");
	d.storage("Maximo", "255", "long");
	d.storage("Minimo", "0", "int");
	d.storage("Desvio Padrao", "78.7", "float");
	
	cout << "Total de registro: " << d.size() << endl;
	sd = d.getLastElement();
	cout << "ultimo elemento: " <<  sd->name << " " <<
		sd->value << " " << sd->type << endl;
	sd = d.getElement(1);
	cout << "Elemento 1 = " << sd->name << " " <<
			sd->value << " " << sd->type << endl;
	sd = d.getElement();
	while ( sd != (Data *)(NULL) )
	{
		cout << "Elemento  [ " << d.getPos() << " ] = " << sd->name << " " <<
			sd->value << " " << sd->type << endl;
		sd = d.getElement();
	}
	d.save();
	
	return 0;
}
