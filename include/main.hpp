#ifndef __MAIN_HPP__
#	define __MAIN_HPP__

#	include <iostream>
#	include <string>

using namespace std;


extern bool debugflag;
extern string debugstr;


#	define	ERR_MSG(s,n)			cerr << (s) << ": " << (n) << flush << endl;
#	define	MSG(msg,a,b,c)			cout << (msg) << " " << (a) << " " << (b) << " " << (c) << flush << endl;
#	define	VERBOSE(msg,a,b)		if ( verboseflag ) cout << msg  << " " << a << " " << b << flush << endl;
#	define	DEBUG(f,msg)			if ( debugflag ) cout << debugstr << (f) << (msg) << flush << endl;
#	define	DEBUGM(f,msg,a,b,c,d,e)	if ( debugflag ) cout << debugstr << "Function: " << (f) << (msg) << " " << (a) << " " << (b) << " " << (c) << " " << (d) << " " << (e) << endl;
#	define	ERRMSG(f,msg)			cout << f << ": Error: " << (msg) << ":" << endl; 
#	define	EMPTY(foo,img,r)		if ( img.empty() ){ cout << foo << ": Image Empty!" << endl << flush ; return r;}
#	define	EMPTY_V(foo,img)		if ( img.empty() ){ cout << foo << ": Image Empty!" << endl << flush ;}
#	define	EXIT_IFNOT(v,msg,a,b)	if ( !v ) { ERR_MSG(msg,a); exit(b); }  
#	define	EXIT_IF(v,msg,a,b)		if (  v ) { ERR_MSG(msg,a); exit(b); }  



#endif
